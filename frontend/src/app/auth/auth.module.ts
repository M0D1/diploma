import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthViewComponent } from './auth-view/auth-view.component';
import { LoginComponent } from './components/login/login.component';


@NgModule({
  declarations: [
    AuthViewComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule
  ]
})
export class AuthModule { }
