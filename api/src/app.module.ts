import { DynamicModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from './auth/auth.module';
import { devOrmConfig, ormConfig } from './config';
import { AdminModule } from './admin/admin.module';
import { AppointmentsModule } from './appointments/appointments.module';
import { PatientModule } from './patient/patient.module';
import { DoctorModule } from './doctor/doctor.module';
import { FakerModule } from './faker/faker.module';

import { UsersModule } from './users/users.module';
import { SecretaryModule } from './secretary/secretary.module';


@Module({
  imports: [
    ThrottlerModule.forRoot({
      ttl: 60,
      limit: 5,
    }),
    ConfigModule.forRoot({ isGlobal: true }),
    OrmModule(process.env.MODE === 'production'),
    AuthModule,
    AdminModule,
    PatientModule,
    AppointmentsModule,
    DoctorModule,
    FakerModule,
    SecretaryModule,
    UsersModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }

export function OrmModule(production = false): DynamicModule {
  if (production) {
    return TypeOrmModule.forRoot(ormConfig.getTypeOrmConfig());
  } else {
    return TypeOrmModule.forRootAsync(devOrmConfig);
  }
}
