import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { UserRole } from 'src/users/models/user-roles.enum';
import { faker } from '@faker-js/faker/locale/ru';
import { AuthorizedEntity } from 'src/auth/entities';
import { v4 as UUID } from "uuid"
import { UserStatus } from 'src/auth/models';
import { UserEntity } from 'src/users/entities/user.entity';
import { AppointmentEntity } from 'src/appointments/entity';
import { RoleEntity, RoleStatus } from 'src/auth/entities/roles.entity';
import { DoctorEntity } from 'src/doctor/entity/doctor.entity';
import { SecretaryEntity } from 'src/secretary/entities/secretary.entity';
import { PatientEntity } from 'src/patient/entity/patient.entity';
import { MedicalTestEntity } from 'src/patient/entity/medical-test.entity';
import { Faker } from '@faker-js/faker';
const random = (min: number = 0, max: number = 1) => Math.floor(Math.random() * (max - min)) + min;
function randomDateAfterDate(start: Date, days: number) { return new Date(start.getTime() + (Math.random() * days * 24 * 60 * 60 * 1000)); }

@Injectable()
export class FakerService implements OnModuleInit {
    async onModuleInit() {
        // await this.generateAdminUser();
        // await this.generateFakeData();
        // await this.generateForAdmin()
    }

    private async generateForAdmin() {
        let admin = await AuthorizedEntity.findOne({ where: { email: 'zakout.mo@gmail.com' }, relations: ["profile"] });

        let doctors = await DoctorEntity.find();

        let appointments: AppointmentEntity[] = [];
        let rD = doctors[random(0, doctors.length - 1)];
        let extra = new MedicalTestEntity({ content: faker.lorem.words(5), notices: faker.lorem.words(10), medications: [faker.lorem.word()] });
        for (let i = 0; i < 20; i++) {
            let appointment: AppointmentEntity = new AppointmentEntity({
                doctorId: rD.id,
                patientId: admin.id,
                startDate: randomDateAfterDate(new Date(Date.now()), random(0, 60)),
                startTime: `${random(0, 12)}:${random(0, 59)}`,

            });
            appointments.push(appointment);

        }
        try {
            await AppointmentEntity.save(appointments);
        } catch (e) { Logger.error(e) }
    }
    private async generateFakeData() {
        Logger.log(`Generate Admin User`)
        await this.generateAdminUser();

        Logger.log('Generating Doctors start');
        await this.generateUsers(UserRole.DOCTOR, 10);

        Logger.log('Generating Secretaries start')
        await this.generateUsers(UserRole.SECRETARY, 10);

        Logger.log('Generating Patients start')
        await this.generateUsers(UserRole.PATIENT, 30);

        Logger.log('Generating Appointments started')
        await this.generateAppointments();
    }

    private async generateUsers(role: UserRole, count: number = 20) {
        const authorized: AuthorizedEntity[] = [];
        let fakeProfiles = [];
        let roles = [];
        for (let i = 0; i < count; i++) {
            let sid = UUID();
            let name = faker.name.firstName();
            let surname = faker.name.lastName();
            let patronymic = faker.name.middleName();
            let username = `${name}${surname}`.toLocaleLowerCase();
            let region = faker.address.cityName();
            let address = faker.address.street();
            let password = `${name}@2@${surname}`;
            let state = faker.address.state();
            let phone = faker.phone.phoneNumber();
            let dob = faker.date.between('1950-01-01T00:00.000Z', '1995-01-01T00:00.000Z').toString();
            let status = UserStatus.ACTIVE;

            const credentials = new AuthorizedEntity({
                id: sid,
                username,
                status,
                email: `${name}${surname}@mail.ru`,
                password,
                profile: new UserEntity({ id: sid, name, patronymic, phone, region, surname, address, dob })
            });
            roles.push(new RoleEntity({ role, userId: sid }));
            switch (role) {
                case UserRole.DOCTOR:
                    fakeProfiles.push(new DoctorEntity({ id: sid }));
                    break;
                case UserRole.PATIENT:
                    fakeProfiles.push(new PatientEntity({ id: sid }));
                    break;
            }

            authorized.push(credentials);
        }

        await AuthorizedEntity.save(authorized);
        switch (role) {
            case UserRole.DOCTOR:
                await DoctorEntity.save(fakeProfiles);
                break;
            case UserRole.SECRETARY:
                await SecretaryEntity.save(fakeProfiles);
                break;
            case UserRole.PATIENT:
                await PatientEntity.save(fakeProfiles);
                break;
        }
        await RoleEntity.save(roles);
    }

    private async generateAdminUser() {
        let admin: AuthorizedEntity = await AuthorizedEntity.findOne({
            where: { username: 'admin' },
        });
        if (!admin) {
            admin = new AuthorizedEntity({
                username: 'admin',
                password: 'admin',
                status: UserStatus.ACTIVE,
                email: 'zakout.mo@gmail.com',
                profile: new UserEntity({
                    name: 'Mohammad',
                    surname: 'Zakout',
                    region: 'proletarsky',
                    address: 'Sholokhova94',
                    patronymic: 'N. M.',
                }),
            });
            admin = await admin.save();
            let role1 = new RoleEntity({ user: admin, role: UserRole.SUPER, status: RoleStatus.APPROVED });
            let role2 = new RoleEntity({ user: admin, role: UserRole.PATIENT, status: RoleStatus.APPROVED });
            let role3 = new RoleEntity({ user: admin, role: UserRole.DOCTOR, status: RoleStatus.APPROVED });
            let doctor = new DoctorEntity({ user: admin.profile, department: '' });
            doctor = await doctor.save();
            let patient = new PatientEntity({ user: admin.profile })
            patient = await patient.save();
            let role4 = new RoleEntity({ user: admin, role: UserRole.ADMINISTRATOR, status: RoleStatus.APPROVED });
            await RoleEntity.save([role1, role2, role3, role4]);
            // await role.save();
        }
    }
    private async generateAppointments(count: number = 250) {
        let doctors = await DoctorEntity.find();
        // let secretaries = await SecretaryEntity.find();
        let patients = await PatientEntity.find();

        let appointments: AppointmentEntity[] = [];


        for (let i = 0; i < count; i++) {
            //random doctor
            let rD = doctors[random(0, doctors.length - 1)];

            let rP = patients[random(0, patients.length - 1)]

            let appointment: AppointmentEntity = new AppointmentEntity({
                doctorId: rD.id,
                // secretaryId: rS.id,
                patientId: rP.id,
                startDate: randomDateAfterDate(new Date(Date.now()), random(0, 60)),
                startTime: `${random(0, 12)}:${random(0, 59)}`,
            });
            appointments.push(appointment);

        }

        try { await AppointmentEntity.save(appointments); } catch (e) { Logger.error(e) }
    }
}