import { Body, DefaultValuePipe, Delete, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiQuery } from '@nestjs/swagger';
import { AppointmentStatus } from 'src/appointments/models/appointment.enum';
import { GetUser, Roles } from 'src/auth/decorators';
import { JwtAuthGuard, RolesGuard } from 'src/auth/guards';
import { ActiveGuard } from 'src/auth/guards/status.guard';
import { UserPayload } from 'src/auth/models';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { UserRole } from 'src/users/models/user-roles.enum';

import { DoctorService } from './doctor.service';

@CustomController('doctor')
// @Roles(UserRole.DOCTOR)
export class DoctorController {
    constructor(private readonly service: DoctorService) { }

    // @Post()
    // @ApiOperation({ summary: 'Create Doctor Profile' })
    // async create(@GetUser() user: UserPayload) {
    //     return this.service.create(user);
    // }
    @Get('appointments')
    @ApiQuery({ name: 'status', type: 'enum', enum: AppointmentStatus, required: false })
    @ApiQuery({ name: 'date', type: String, description: 'start Date', required: false })
    @ApiQuery({ name: 'time', type: String, description: 'start time', required: false })
    @ApiOperation({ summary: 'Get assgined appointments' })
    async findAppointments(
        @GetUser() user: UserPayload,
        @Query('status') status: AppointmentStatus,
        @Query('date', new DefaultValuePipe(null)) date: string,
        @Query('time', new DefaultValuePipe(null)) time: string,
    ) {
        return this.service.fetchMyAppointments(user);
    }

    @Get('')
    @ApiOperation({ summary: 'Get Doctor profile', description: 'Returns doctor profile of the current autherized user' })
    async findOne(@GetUser() user: UserPayload) {
        return this.service.me(user);
    }


    // @Put(':id')
    // // @Roles(UserRole.DOCTOR)
    // // @UseGuards(ActiveGuard)
    // @ApiOperation({ summary: 'Update doctor profile' })
    // async update(@Param('id') id: string, @Body() updateSecretaryDto: any) {
    //     return this.service.update(+id, updateSecretaryDto);
    // }

    // @Delete(':id')
    // // @Roles(UserRole.DOCTOR)
    // // @UseGuards(ActiveGuard)
    // @ApiOperation({ summary: 'Delete doctor profile' })
    // async remove(@GetUser() user: UserPayload) {
    //     return this.service.remove(user);
    // }



}
