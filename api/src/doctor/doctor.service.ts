import { BadRequestException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { doc } from 'prettier';
import { NotFoundError } from 'rxjs';
import { AppointmentEntity } from 'src/appointments/entity';
import { RoleEntity, RoleStatus } from 'src/auth/entities';
import { UserPayload } from 'src/auth/models';
import { UserRole } from 'src/users/models/user-roles.enum';
import { Repository } from 'typeorm';
import { DoctorEntity } from './entity/doctor.entity';

@Injectable()
export class DoctorService {

    constructor(
        @InjectRepository(DoctorEntity)
        private readonly repo: Repository<DoctorEntity>
    ) { }

    async create(user: UserPayload) {
        let userDoctorRole = await RoleEntity.findOne({ where: { userId: user.id, role: UserRole.DOCTOR } });
        if (userDoctorRole) throw new BadRequestException('error.already_have_doctor_profile')
        let doctorProfile: DoctorEntity = new DoctorEntity({ id: user.id });
        let role: RoleEntity = new RoleEntity({ role: UserRole.DOCTOR, userId: user.id });
        try {
            await role.save();
            await doctorProfile.save();
            return { message: 'Your request is now under admin review' };
        } catch (e) { throw e }

    }

    async me(user: UserPayload) {
        const { id, roles } = user;
        if (roles.includes(UserRole.ADMINISTRATOR) && !roles.includes(UserRole.DOCTOR)) {
            return { message: 'Please use the Admin endpoints' }
        }
        return await this.findOne(id);

    }

    async findOne(id: string) {
        let doctorProfile = await DoctorEntity.findOne({ where: { id: id } })
        if (!doctorProfile) throw new NotFoundException();
        let roleStatusCheck = await RoleEntity.findOne({ where: { userId: id, role: UserRole.DOCTOR } });
        if (roleStatusCheck.status == RoleStatus.PENDING) {
            return { message: 'Your profile is still under revision , come back later', status: HttpStatus.OK }
        }
        return doctorProfile;
    }

    update(id: number, updateSecretaryDto: any) {
        return `This action updates a  authorized user doctor profile doctor`;
    }

    remove(user: UserPayload) {
        return `This action removes authorized user doctor profile doctor`;
    }

    async fetchMyAppointments(user: UserPayload) {

        let qb = AppointmentEntity.createQueryBuilder('a');
        qb.leftJoinAndSelect('a.patient', 'patient');
        qb.leftJoinAndSelect('patient.user', 'patientProfile');
        qb.select(['a.id','a.status', 'a.startTime', 'a.startDate', 'patient', 'patientProfile.name', 'patientProfile.surname', 'patientProfile.gender', 'patientProfile.dob'])

        return qb.getMany();
    }

}
