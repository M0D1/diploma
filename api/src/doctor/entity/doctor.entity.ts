import { ApiHideProperty } from "@nestjs/swagger";
import { Exclude } from "class-transformer";
import { AppointmentEntity } from "src/appointments/entity";
import { UserEntity } from "src/users/entities/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryColumn } from "typeorm";
export enum Department {
    none = '',
    dermatology = "dermatology",
    dentistry = "dentistry",
    psychiatry = "psychiatry",
    pediatrics_and_new_born = "pediatrics and new born",
    neurology = "neurology",
    orthopedic = "orthopedic",
    gynecology_and_infertility = "gynecology and infertility",
    ear_nose_and_throat = "ear nose and throat",
    cardiology_and_vascular_diseases = "cardiology and vascular diseases"
}

@Entity('doctors')
export class DoctorEntity extends BaseEntity {
    @PrimaryColumn('uuid')
    id: string;

    @Column({ type: 'enum', enum: Department, default: Department.none, nullable: true })
    department: string;

    @Column({ default: '' })
    office: string;
    
    @Column({ type: 'jsonb', default: '{}', nullable: true })
    workingHours: string;
    @Column({ type: 'jsonb', default: '{}', nullable: true })
    workingdDays: string;

    @OneToOne(() => UserEntity, user => user, { nullable: true })
    @JoinColumn({ name: 'id', referencedColumnName: 'id' })
    @ApiHideProperty()
    user: UserEntity;

    @ApiHideProperty()
    @OneToMany(() => AppointmentEntity, a => a.doctor)
    appointments: AppointmentEntity[];

    constructor(partial?: Partial<DoctorEntity>) {
        super();
        if (partial) Object.assign(this, partial);
    }
}