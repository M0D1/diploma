import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SearchQuery } from 'src/admin/models/appointment.search';
import { AppointmentEntity } from 'src/appointments/entity';
import { UserPayload } from 'src/auth/models';
import { MedicalTestEntity } from 'src/patient/entity/medical-test.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RecordsService {


    constructor(
        @InjectRepository(AppointmentEntity)
        private readonly repo: Repository<AppointmentEntity>
    ) { }


    async getMedicalRecords(user: UserPayload, date: string = "", pId: string = ""): Promise<MedicalTestEntity[]> {
        let result = await this.repo.find({ where: { patientId: user.id } });
        let medicalRecords: MedicalTestEntity[] = [];
        return medicalRecords
    }

    getMedicalRecordSecretary(user: UserPayload, sQuery: SearchQuery) {
        throw new Error('Method not implemented.');
    }
}
