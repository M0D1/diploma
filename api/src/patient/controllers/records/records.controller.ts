import { Controller, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiQuery } from '@nestjs/swagger';
import { SearchQuery } from 'src/admin/models/appointment.search';
import { AppointmentEntity } from 'src/appointments/entity';
import { GetUser, Roles } from 'src/auth/decorators';
import { UserPayload } from 'src/auth/models';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { MedicalTestEntity } from 'src/patient/entity/medical-test.entity';
import { RecordsService } from 'src/patient/services/records/records.service';
import { UserRole } from 'src/users/models/user-roles.enum';

@CustomController('medica-records')
export class RecordsController {

    constructor(private readonly service: RecordsService) { }

    @Get('doctor')
    @ApiQuery({ name: 'patientId', type: String, required: true })
    @ApiOperation({ summary: 'Get full Patient medical records', description: 'This endpoint is used by the doctors to fetch the medical record of a patient' })
    @ApiOkResponse({ isArray: true, type: MedicalTestEntity })
    async getMedicalRecords(@GetUser() user: UserPayload, @Query('patientId') pId: string) {
        return this.service.getMedicalRecords(user, pId);
    }

    @Get("patient")
    @ApiQuery({ type: String, name: 'date', required: false, example: 'yyyy-mm-dd' })
    @ApiOperation({ summary: 'Get Patient medical records', description: 'This endpoing is used by the patient to return his own medical record' })
    async getMedicalRecord(
        @GetUser() user: UserPayload,
        @Query('date') date: string
    ) {
        return this.service.getMedicalRecords(user, date, "");
    }

    @Get("secretary")
    @ApiQuery({ name: 'doctorId', type: String, required: false })
    @ApiQuery({ name: 'patientId', type: String, required: false })
    @ApiQuery({ type: String, name: 'date', required: false, example: 'yyyy-mm-dd' })
    @ApiOperation({ summary: 'Get medical records', description: 'Return medical records made by a doctor or related to a patient' })
    @Roles(UserRole.SUPER)
    async getMedicalRecordSecretary(
        @GetUser() user: UserPayload,
        @Query() sQuery: SearchQuery
    ) {
        return this.service.getMedicalRecordSecretary(user, sQuery);
    }

}
