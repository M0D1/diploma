import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PatientEntity } from './entity/patient.entity';
import { PatientController } from './patient.controller';
import { PatientService } from './patient.service';
import { RecordsController } from './controllers/records/records.controller';
import { RecordsService } from './services/records/records.service';
import { MedicalTestEntity } from './entity/medical-test.entity';
import { AppointmentEntity } from 'src/appointments/entity';

@Module({
  imports: [TypeOrmModule.forFeature([PatientEntity, AppointmentEntity, MedicalTestEntity])],
  controllers: [PatientController, RecordsController],
  providers: [PatientService, RecordsService]
})
export class PatientModule { }
