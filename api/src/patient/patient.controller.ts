import { Body, Get, Post, Put } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiOperation } from '@nestjs/swagger';
import { GetUser, Roles } from 'src/auth/decorators';
import { JwtAuthGuard } from 'src/auth/guards';
import { UserPayload } from 'src/auth/models';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { UserRole } from 'src/users/models/user-roles.enum';
import { PatientService } from './patient.service';

@CustomController('patient')
@Roles(UserRole.PATIENT)
export class PatientController {

    constructor(private readonly service: PatientService) { }

    @Get('me')
    @ApiOperation({ summary: 'Get Profile', description: 'Returns patient profile of the current autherized user' })
    async getProfile(@GetUser() user: UserPayload) {
        return this.service.me(user);
    }

    @Get('appointments')
    @ApiOperation({ summary: 'Get Pateint Appointments', description: 'Return the appointments of the current autherized user' })
    async getAppointments(@GetUser() user: UserPayload) {
        return this.service.myAppointments(user);
    }
    // @Post()
    // @ApiOperation({ summary: 'Create Patient Profile' })
    // async createProfile(@GetUser() user: UserPayload) {
    //     return this.service.create(user);
    // }

    // @Put()
    // @ApiOperation({ summary: 'Update Profile', description: 'Return the current' })
    // async updateProfile(@GetUser() user: UserPayload, @Body() data) { }

}
