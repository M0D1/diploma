import { BadRequestException, HttpStatus, Injectable, Logger, NotFoundException, } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AppointmentEntity } from 'src/appointments/entity';
import { RoleEntity, RoleStatus } from 'src/auth/entities';
import { UserPayload } from 'src/auth/models';
import { UserRole } from 'src/users/models/user-roles.enum';
import { Repository } from 'typeorm';
import { PatientEntity } from './entity/patient.entity';

@Injectable()
export class PatientService {
    constructor(@InjectRepository(PatientEntity) private readonly repo: Repository<PatientEntity>) { }
    async me(user: UserPayload): Promise<PatientEntity> {

        return await this.findOne(user.id)

    }

    async create(user: UserPayload) {
        let roleDB = await RoleEntity.findOne({ where: { userId: user.id, role: UserRole.PATIENT } });
        if (roleDB) throw new BadRequestException('error.already_have_patient_profile')
        let patient: PatientEntity = new PatientEntity({ id: user.id });
        let role: RoleEntity = new RoleEntity({ role: UserRole.PATIENT, userId: user.id });
        try {
            await role.save();
            await this.repo.save(patient)
            return { message: 'Your request is now under admin review' };
        } catch (e) { throw e }
    }


    async myAppointments(user: UserPayload) {
        return await AppointmentEntity.find({ where: { patientId: user.id } })
    }

    async findOne(id: string) {
        let profile = await this.repo.findOne({ where: { id } })
        Logger.log(JSON.stringify(profile))
        if (!profile) throw new NotFoundException();
        // let roleStatusCheck = await RoleEntity.findOne({ where: { userId: sid, role: UserRole.PATIENT } });
        // if (roleStatusCheck.status == RoleStatus.PENDING) {
        //     return { message: 'Your profile is still under revision , come back later', status: HttpStatus.OK }
        // }
        return profile;
    }

}
