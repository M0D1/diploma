import { ApiHideProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Exclude } from "class-transformer";
import { AppointmentEntity } from "src/appointments/entity";
import { BaseEntity, Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { PatientEntity } from "./patient.entity";

@Entity('medical_analyses')
export class MedicalTestEntity extends BaseEntity {
    @Exclude()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', default: '', nullable: true })
    name: string;

    @Column({ type: 'uuid' })
    patientId: string;

    @ManyToOne(() => PatientEntity, patient => patient.tests, { nullable: false, onDelete: 'SET NULL' })
    @ApiHideProperty()
    patient: PatientEntity;

    @ManyToOne(() => AppointmentEntity, patient => patient.extra, { nullable: false, onDelete: 'SET NULL' })
    @JoinColumn({ name: 'appointmentId', referencedColumnName: 'id' })
    appointment: AppointmentEntity;

    @Column({ type: 'jsonb', default: '{}' })
    @ApiPropertyOptional({ type: JSON })
    content: string;

    @Column({ type: 'jsonb', default: '{}' })
    @ApiPropertyOptional({ type: JSON })
    diagnostics: string;

    @Column({ type: 'jsonb', default: '{}' })
    @ApiPropertyOptional({ type: JSON })
    medications: string[];

    @Column({ type: 'jsonb', default: '{}' })
    @ApiPropertyOptional({ type: JSON })
    notices: string;

    @CreateDateColumn()
    @ApiPropertyOptional({ type: Date })
    createdAt: Date;

    constructor(partial?: Partial<MedicalTestEntity>) {
        super();
        if (partial) Object.assign(this, partial);
    }
}