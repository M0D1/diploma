import { ApiHideProperty, ApiProduces, ApiProperty } from "@nestjs/swagger";
import { AppointmentEntity } from "src/appointments/entity";
import { UserEntity } from "src/users/entities/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryColumn, UpdateDateColumn } from "typeorm";
import { MedicalTestEntity } from "./medical-test.entity";

@Entity('patients')
export class PatientEntity extends BaseEntity {
    @PrimaryColumn('uuid')
    id: string;

    @Column({ default: '' })
    bloodCategory: string;

    @Column({ type: 'boolean', default: false })
    hasSTD: boolean;
    @Column({ type: 'boolean', default: false })
    hasPerminitDesiece: boolean;
    @Column({ type: 'boolean', default: false })
    hasAllergies: boolean

    // @Column()
    @ApiProperty({type:Boolean})
    upcoming: boolean;
    @UpdateDateColumn({ nullable: true })
    lastMedicalCheck: Date;
    @OneToOne(() => UserEntity, user => user)
    @JoinColumn({ name: 'id', referencedColumnName: 'id' })
    @ApiHideProperty()
    user: UserEntity;

    @OneToMany(() => MedicalTestEntity, tests => tests.patient, { onDelete: 'CASCADE' })
    @ApiHideProperty()
    tests: MedicalTestEntity;

    @OneToMany(() => AppointmentEntity, appointment => appointment.patient)
    @ApiHideProperty()
    appointments: AppointmentEntity[];


    constructor(partial?: Partial<PatientEntity>) {
        super();
        if (partial) Object.assign(this, partial);
    }

}