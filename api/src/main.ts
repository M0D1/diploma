import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { GlobalExceptionFilter } from './common/exceptions/global.exception';
import { TimeInterceptor } from './common/interceptors/time.interceptor';
const PORT = process.env.SERVER_PORT || 3000;
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: {
      origin: ['localhost', '127.0.0.1', 'diploma'],
      methods: 'GET,PUT,POST,DELETE',
      credentials: true,
    },
  });
  app.setGlobalPrefix('/api/v1');
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.useGlobalInterceptors(new TimeInterceptor());
  app.useGlobalFilters(new GlobalExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      skipMissingProperties: false,
    }),
  );
  await initSwaggerModule(app);
  await app.listen(PORT);
}

bootstrap();

async function initSwaggerModule(app: NestExpressApplication) {
  const config = new DocumentBuilder()
    .setTitle('Diploma API')
    .setDescription('API description')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
}
