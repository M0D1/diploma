import { Body, Get, Put } from '@nestjs/common';
import { UsersService } from './users.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { GetUser } from 'src/auth/decorators';
import { UserPayload } from 'src/auth/models';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { ProfileDTO } from './dto/profile.dto';

@CustomController('Users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Get('me')
  @ApiOperation({ summary: 'Get Profile', description: 'Return the general profile of the authorized user' })
  findOne(@GetUser() user: UserPayload) {
    return this.usersService.findOne(user);
  }

  @Put('me')
  @ApiOperation({ summary: 'Update Profile' })
  @ApiOkResponse({ type: ProfileDTO })
  async update(@GetUser() user: UserPayload, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(user, updateUserDto);
  }
}
