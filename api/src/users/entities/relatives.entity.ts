import { ApiHideProperty } from "@nestjs/swagger";
import { Exclude } from "class-transformer";
import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";
@Entity('relatives')
export class RelativesEntity extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'uuid', nullable: true })
    public personId: string;

    @Column({ type: 'uuid', nullable: true })
    public fatherId: string;

    @Column({ type: 'uuid', nullable: true })
    public motherId: string;

    @Exclude()
    @OneToOne(() => UserEntity, person => person.relatives)
    @ApiHideProperty()
    person: UserEntity;

    @Exclude()
    @ManyToOne(() => UserEntity, { nullable: true })
    @JoinColumn({ name: 'fatherId', referencedColumnName: 'id' })
    @ApiHideProperty()
    father: UserEntity;

    @Exclude()
    @ManyToOne(() => UserEntity, { nullable: true })
    @JoinColumn({ name: 'motherId', referencedColumnName: 'id' })
    @ApiHideProperty()
    mother: UserEntity;

}