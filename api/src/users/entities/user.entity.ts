import { Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from 'typeorm';
import { BaseProfileEntity } from 'src/common/entity/profile.entity';
import { AuthorizedEntity } from '../../auth/entities/authorized.entity';
import { Exclude } from 'class-transformer';
import { ApiHideProperty } from '@nestjs/swagger';
import { RelativesEntity } from './relatives.entity';
import { DoctorEntity } from 'src/doctor/entity/doctor.entity';
import { PatientEntity } from 'src/patient/entity/patient.entity';

@Entity('users')
export class UserEntity extends BaseProfileEntity {
  @Exclude()
  @ApiHideProperty()
  @PrimaryColumn('uuid')
  id: string;

  @Exclude()
  @OneToOne(() => AuthorizedEntity, (auth) => auth.profile, { nullable: false })
  @JoinColumn({ name: 'id', referencedColumnName: 'id' })
  @ApiHideProperty()
  auth: AuthorizedEntity;


  @OneToOne(() => RelativesEntity, relative => relative.person, { nullable: true, onDelete: 'CASCADE' })
  @JoinColumn({ name: 'relativesId', referencedColumnName: 'id' })
  @ApiHideProperty()
  relatives: RelativesEntity;


  @OneToOne(() => DoctorEntity, e => e.user, { nullable: true, cascade: true })
  @ApiHideProperty()
  doctorProfile: DoctorEntity;

  @OneToOne(() => PatientEntity, e => e.user, { nullable: true, cascade: true })
  @ApiHideProperty()
  patientProfile: PatientEntity;


  constructor(partial?: Partial<UserEntity>) {
    super(partial);
    if (partial) Object.assign(this, partial);
  }
}
