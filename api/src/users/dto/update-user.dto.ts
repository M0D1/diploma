import { PartialType } from '@nestjs/swagger';
import { ProfileDTO } from './profile.dto';

export class UpdateUserDto extends PartialType(ProfileDTO) {}
