import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional, IsPhoneNumber, IsString, MinLength } from 'class-validator';
import { Gender } from '../models/gender.enum';

export class ProfileDTO {
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  @ApiProperty({ type: String, title: 'First name', minLength: 1, maxLength: 255 })
  name: string;
  @IsString()
  @IsNotEmpty()
  @MinLength(1)
  @ApiProperty({ type: String, title: 'Family name', minLength: 1, maxLength: 255 })
  surname: string;
  @IsString()
  @IsNotEmpty()
  @MinLength(0)
  @IsOptional()
  @ApiProperty({ type: String, title: 'middle name', minLength: 0, maxLength: 255, required: false })
  patronymic: string;
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, title: 'Address', maxLength: 255 })
  address: string;
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, title: 'Region', maxLength: 255 })
  region: string;
  @IsNotEmpty()
  @IsPhoneNumber()
  @ApiProperty({ type: String, title: 'Phone number', maxLength: 21 })
  phone: string;
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String, title: 'date of birth', example: 'dd-mm-yyyy' })
  dob: string;
  @IsNotEmpty()
  @IsEnum(Gender)
  @ApiProperty({ type: 'enum', enum: Gender, title: 'gender' })
  gender: Gender;
}
