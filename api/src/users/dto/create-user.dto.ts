import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsObject } from 'class-validator';
import { RegisterDTO } from 'src/auth/dto/auth.dto';
import { ProfileDTO } from './profile.dto';

export class CreateUserDto extends ProfileDTO {
  @Type(() => RegisterDTO)
  @IsNotEmpty()
  @IsObject()
  @ApiProperty({ type: () => RegisterDTO })
  credentials: RegisterDTO;
}
