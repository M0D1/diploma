import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserPayload } from 'src/auth/models';
import { Repository } from 'typeorm';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(UserEntity)
    private repo: Repository<UserEntity>
  ) { }
  async findOne(user: UserPayload) {
    let userDB = await this.repo.findOne({
      where: { id: user.id }
    })
    if (!userDB) throw new NotFoundException('error.profile_not_found')
    return userDB;
  }

  async update(user: UserPayload, data: UpdateUserDto) {
    let userDB = await this.findOne(user);
    Object.assign(userDB, data);
    userDB = await userDB.save();
    return userDB;
  }


  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
