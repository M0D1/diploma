import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { AppointmentStatus } from '../models/appointment.enum';

export class AppointmentDTO {
  @IsUUID()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, name: 'doctorId' })
  doctorId: string;

  @IsUUID()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String, name: 'patientId' })
  patientId: string;

  @ApiProperty({ type: Date, name: 'date' })
  @IsNotEmpty()
  date: string;
  @IsNotEmpty()
  @ApiProperty({ type: Date, name: 'time' })
  time: string;
}

export class CreateAppointment extends AppointmentDTO {
  @ApiHideProperty()
  readonly status: AppointmentStatus = AppointmentStatus.PENDING;
}
export class UpdateAppointment extends CreateAppointment { }
