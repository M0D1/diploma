import { ApiHideProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { DoctorEntity } from 'src/doctor/entity/doctor.entity';
import { MedicalTestEntity } from 'src/patient/entity/medical-test.entity';
import { PatientEntity } from 'src/patient/entity/patient.entity';
import { SecretaryEntity } from 'src/secretary/entities/secretary.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,

  OneToOne,

  PrimaryGeneratedColumn,
} from 'typeorm';
import { AppointmentStatus } from '../models/appointment.enum';


export class Details {
  diagnostic: string;
  medication: string;
  recommendations: string;
}

@Entity('appointments')
export class AppointmentEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'enum', enum: AppointmentStatus, default: AppointmentStatus.PENDING })
  status: AppointmentStatus;

  @Column({ type: 'date', nullable: true })
  startDate: Date;

  @Column({ type: 'time', nullable: true })
  startTime: string;

  @Column({ type: 'uuid', nullable: true })
  @ApiHideProperty()
  public doctorId: string;

  @Column({ type: 'uuid', nullable: true })
  @ApiHideProperty()
  public patientId: string;


  @ManyToOne(() => DoctorEntity)
  @JoinColumn({ name: 'doctorId', referencedColumnName: 'id' })
  @ApiHideProperty()
  doctor: DoctorEntity;

  @ManyToOne(() => PatientEntity, p => p.appointments)
  @JoinColumn({ name: 'patientId', referencedColumnName: 'id' })
  @ApiHideProperty()
  patient: PatientEntity;


  @OneToOne(() => MedicalTestEntity, t => t.appointment, { nullable: true })
  extra: MedicalTestEntity;

  constructor(partial?: Partial<AppointmentEntity>) {
    super();
    if (partial) Object.assign(this, partial);
  }
}
