import { Injectable, NotFoundException, } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserPayload } from 'src/auth/models';
import { BadRelationException } from 'src/common/exceptions/relation.exception';
import { Department, DoctorEntity } from 'src/doctor/entity/doctor.entity';
import { Repository } from 'typeorm';
import { CreateAppointment } from './dto/appointment.dto';
import { AppointmentEntity } from './entity/appointment.entity';
@Injectable()
export class AppointmentsService {

  constructor(
    @InjectRepository(AppointmentEntity)
    private readonly repo: Repository<AppointmentEntity>
  ) { }

  async fetchMyAppointments(user: UserPayload) {
    let result = await this.repo.find({ where: [{ patientId: user.id }, { doctorId: user.id }] })
    return result;
  }

  async fetchAppointment(id: number, user: UserPayload) {
    let result = await this.repo.find({ where: [{ patientId: user.id, id }, { doctorId: user.id, id }] })
    if (!result) throw new NotFoundException();
    return result;
  }

  async searchAppointment(date: string, department: Department | string) {
    let _date = new Date(Date.parse(date));
    if (!_date)
      throw new BadRelationException('error.invalid_search_date')
    if (inPast(_date))
      throw new BadRelationException('error.date_in_past')

    let doctors = await DoctorEntity.find({ where: { department }, relations: ["appointments"] });
    if (!doctors) {
      throw new BadRelationException('error.no_available_doctors')
    }
    let availableSlots = [];
    // //TODO .

    for (let i = 0; i < 12; i++) {
      availableSlots.push({ date: _date, duration: 1800, available: i % 2 == 0 })
    }
    let doctorss = await DoctorEntity.find({
      relations: ["user"],
      select: {
        id: true,
        department: true,
        user: {
          name: true,
          patronymic: true,
          surname: true
        }
      }
    })


    let res = doctorss.map(d => {
      return { ...d, slots: availableSlots }
    })

    return res;

  }

  async updateAppointment(id: number, user: any, data: any) {
    throw new Error('Method not implemented.');
  }

  async createAppointment(user: UserPayload, data: CreateAppointment) {
    throw new Error('Method not implemented.');
  }
}

export const inPast = (date: Date) => {
  let now = new Date(Date.now());
  return now.getTime() > date.getTime();
}

export const sameDate = (d1: Date, d2: Date) => {
  return (d1.getDay() == d2.getDay())
    && (d1.getFullYear() == d2.getFullYear())
    && d1.getMonth() == d2.getMonth()
    && d1.getDate() == d2.getDate();
}