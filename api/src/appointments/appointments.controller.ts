import { Body, Get, Param, ParseEnumPipe, ParseIntPipe, Post, Put, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiParam, ApiQuery } from '@nestjs/swagger';
import { GetUser } from 'src/auth/decorators';
import { UserPayload } from 'src/auth/models';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { Department } from 'src/doctor/entity/doctor.entity';
import { AppointmentsService } from './appointments.service';
import { CreateAppointment } from './dto/appointment.dto';
import { AvailableDoctors } from './models/available.class';

@CustomController('appointments')
export class AppointmentsController {
  constructor(private readonly service: AppointmentsService) { }

  @Get()
  async getPersonalAppointments(@GetUser() user: UserPayload) {
    return this.service.fetchMyAppointments(user);
  }

  // @Get('/:id')
  // @ApiParam({ name: 'id', type: Number })
  // async getAppointment(
  //   @GetUser() user: UserPayload,
  //   @Param('id', ParseIntPipe) id: number,
  // ) {
  //   return this.service.fetchAppointment(id, user);
  // }

  @Post('')
  async createAppointment(
    @GetUser() user: UserPayload,
    @Body() data: CreateAppointment
  ) {
    return this.service.createAppointment(user, data);
  }

  @Get('s')
  @ApiOperation({ summary: 'Search Appointments' })
  @ApiQuery({ type: String, name: 'date', example: 'dd-mm-yyyy', required: true })
  @ApiQuery({ type: Date, name: 'time', example: '18:00+00', required: false })
  @ApiQuery({ enum: Department, type: 'enum', name: 'department', required: false })
  @ApiOkResponse({ type: AvailableDoctors, isArray: true })
  async searchAppointment(
    @Query('date') date: string,
    @Query('time') time: string,
    @Query('department') department: string
  ) {
    return await this.service.searchAppointment(date, department);
  }
  @Put('/:id')
  @ApiParam({ name: 'id', type: Number })
  async updateAppointment(
    @GetUser() user: UserPayload,
    @Param('id', ParseIntPipe) id: number,
    @Body() data: any,
  ) {
    return this.service.updateAppointment(id, user, data);
  }
}
