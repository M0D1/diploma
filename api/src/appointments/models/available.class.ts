import { ApiProperty } from "@nestjs/swagger";

class Slot {
    @ApiProperty({ type: Date, example: "13:30:00+00:00" })
    time: string;
    @ApiProperty({ type: Number, example: 1800 })
    duration: number;
    @ApiProperty({ type: Boolean, example: true })
    available: boolean;
}
export class AvailableDoctors {
    @ApiProperty({ type: String })
    id: string;
    @ApiProperty({ type: String })
    name: string;
    @ApiProperty({ type: String })
    surname: string;
    @ApiProperty({ type: String })
    department: string;
    @ApiProperty({ type: Slot, isArray: true })
    slots: Slot[];
}
