import { ApiHideProperty, ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEAN, IsEmail, IsEnum, IsNotEmpty, IsOptional, IsPhoneNumber, IsString, MaxLength, Min, MinLength, ValidateNested } from 'class-validator';
import { UserEntity } from 'src/users/entities/user.entity';
import { Gender } from 'src/users/models/gender.enum';
import { UserRole } from 'src/users/models/user-roles.enum';
export class Profile {
  @ApiProperty({ type: String, title: 'First name', default: '' })
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(255)
  name: string;

  @ApiPropertyOptional({ type: String, title: 'Middle name', default: '' })
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  @MinLength(1)
  @MaxLength(255)
  patronymic: string;

  @ApiProperty({ type: String, title: 'Last name', default: '' })
  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(255)
  surname: string;
  @ApiProperty({ type: 'enum', enum: Gender, nullable: true })
  @IsEnum(Gender)
  @IsNotEmpty()
  gender: Gender;

  @ApiPropertyOptional({ type: String, title: 'Date of birth', example: 'dd-mm-yyyy', nullable: true })
  @IsOptional()
  @IsNotEmpty()
  dob: string;

  @IsOptional()
  @IsPhoneNumber()
  @ApiPropertyOptional({ type: String, title: 'Phone number' })
  phone: string;

  @ApiPropertyOptional({ type: String, title: 'Region' })
  @IsNotEmpty()
  @IsOptional()
  @MinLength(1)
  region: string;

  @ApiPropertyOptional({ type: String, title: 'Address' })
  @IsNotEmpty()
  @IsOptional()
  @MinLength(1)
  address: string;
}

export class RegisterDTO {
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({ type: String, example: 'username', maxLength: 255 })
  username: string;
}

export class LoginDTO extends RegisterDTO {
  @IsString()
  @IsNotEmpty()
  @MaxLength(32)
  @ApiProperty({ type: String, example: 'xxxxxxxxxx', maxLength: 32 })
  password: string;
}


export class RegistrationDTO extends RegisterDTO {
  @IsEmail()
  @IsNotEmpty()
  email: string;
  @ValidateNested()
  @Type(() => Profile)
  @ApiProperty({ type: Profile })
  @IsNotEmpty()
  profile: Profile;
  @IsNotEmpty()
  @ApiHideProperty()
  readonly role: UserRole = UserRole.PATIENT;
}



