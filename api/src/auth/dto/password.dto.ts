import { IsEmail, IsNotEmpty, IsPhoneNumber, ValidateIf } from "class-validator";
export class PasswordResetDTO {
    @IsNotEmpty()
    @IsEmail()
    email: string
}
