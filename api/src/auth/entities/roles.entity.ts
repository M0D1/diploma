import { ApiHideProperty, ApiProduces, ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { makeId } from 'src/common/utils/gen-id';
import { UserRole } from 'src/users/models/user-roles.enum';
import {
    BaseEntity,
    BeforeInsert,
    Column,
    Entity,
    ManyToOne,
    PrimaryColumn,
} from 'typeorm';
import { AuthorizedEntity } from './authorized.entity';

export enum RoleStatus {
    APPROVED = 'APPROVED',
    PENDING = 'PENDING'
}
@Entity('auth_roles')
export class RoleEntity extends BaseEntity {
    @Exclude()
    @PrimaryColumn()
    @ApiHideProperty()
    id: string;

    @Column({ type: 'varchar' })
    public userId: string;

    @Column({ type: 'enum', enum: UserRole })
    @ApiProperty({ type: 'enum', enum: UserRole })
    role: UserRole;

    @Column({ type: 'enum', enum: RoleStatus, default: RoleStatus.PENDING })
    @ApiProperty({ type: 'enum', enum: RoleStatus })
    status: RoleStatus;
    @ManyToOne(() => AuthorizedEntity, (auth) => auth.roles)
    user: AuthorizedEntity;

    constructor(partial?: Partial<RoleEntity>) {
        super();
        if (partial) Object.assign(this, partial);
    }
    @BeforeInsert()
    generateId() {
        this.id = makeId('RL');
    }
}
