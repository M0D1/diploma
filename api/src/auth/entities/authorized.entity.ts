import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { UserEntity } from '../../users/entities/user.entity';
import { UserStatus } from '../models/user-status';
import { Exclude } from 'class-transformer';
import { ApiHideProperty } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { RoleEntity } from './roles.entity';
import { generatePassword } from 'src/common/utils/gen-password';

@Entity('authorized')
export class AuthorizedEntity extends BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', unique: true })
  username: string;
  @Column({ type: 'varchar', unique: true, nullable: false, default: '' })
  email: string;
  @Exclude()
  @Column({ type: 'text', nullable: false })
  @ApiHideProperty()
  password: string;

  @Column({ type: 'enum', enum: UserStatus, default: UserStatus.RESTRICTED })
  status: UserStatus;

  @OneToOne(() => UserEntity, (user) => user.auth, { cascade: true, nullable: true })
  profile: UserEntity;

  @OneToMany(() => RoleEntity, roles => roles.user)
  roles: RoleEntity[];

  constructor(partial?: Partial<AuthorizedEntity>) {
    super();
    if (partial) Object.assign(this, partial);
  }

  @BeforeInsert()
  async hashPassword() {
    if (this.password) {
      const hash = bcrypt.hashSync(this.password, 12);
      this.password = hash;
    }
  }

  public async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }

  public async changePassword(): Promise<string> {
    let newPassword = generatePassword(18);
    const hash = bcrypt.hashSync(newPassword, 12);
    this.password = hash;
    await this.save();
    return newPassword;
  }
}
