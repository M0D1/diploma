import {
  BadRequestException,
  ForbiddenException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UpdateUserStatus } from 'src/admin/dto';
import { UserRole } from 'src/users/models/user-roles.enum';

import { LoginDTO, RegistrationDTO } from './dto/auth.dto';
import { AuthorizedEntity } from './entities/authorized.entity';
import { RoleEntity } from './entities/roles.entity';
import { UserPayload, UserStatus, Token } from './models';
import { UserEntity } from 'src/users/entities/user.entity';
import { PasswordResetDTO } from './dto/password.dto';
import { RevokedTokensEntity } from './entities';
import { generatePassword } from 'src/common/utils/gen-password';
import { PatientEntity } from 'src/patient/entity/patient.entity';


@Injectable()
export class AuthService {
  private expired: string[] = [];
  private readonly logger = new Logger('AuthService');

  constructor(private readonly jwtService: JwtService) { }

  async login(signInDTO: LoginDTO) {
    const { username, password } = signInDTO;
    const authUser = await AuthorizedEntity.findOne({
      where: { username },
      relations: ["roles"]
    });
    const validPassword = await authUser.validatePassword(password);
    if (!authUser || !validPassword)
      throw new BadRequestException('error.username_or_password_is_invalid');
    let roles = authUser.roles.map(n => n.id)
    return this.generateJWT({ id: authUser.id, roles: roles, type: "AC" });
  }

  async generateJWT(user: UserPayload): Promise<Token> {
    const at = await this.jwtService.signAsync({ user });
    return { access_token: at, type: 'Bearer' };
  }

  async updateAuthStatus(id: string, data: UpdateUserStatus) {
    let authUser = await this.findUserById(id)
    Object.assign(authUser, data);
    authUser = await authUser.save();
    return authUser;
  }

  async findUserById(id: string) {
    let authUser = await AuthorizedEntity.findOne({ where: { id } });
    if (!authUser) throw new NotFoundException('error.user_not_found')
    return authUser;
  }

  async changePassword(token: string) {
    let validToken = await this.jwtService.verifyAsync(token);
    let revoked: RevokedTokensEntity = await RevokedTokensEntity.findOne({ where: { token } });
    if (revoked) {
      throw new UnauthorizedException('error.expired_token');
    }

    if (validToken) {
      let { id } = this.jwtService.decode(token) as UserPayload;
      let authUser = await this.findUserById(id);
      if (authUser.status == UserStatus.BLOCKED) {
        throw new ForbiddenException('error.user_blocked')
      }
      let newPassword = await authUser.changePassword();
      Logger.log(`New Generated Password ${newPassword}`);
      this.expired.push(token);
      return { message: 'New password has been sent to your email', status: HttpStatus.CREATED };
    }
  }

  async resetPasswordRequest(data: PasswordResetDTO) {
    const { email } = data;

    let authUser = await AuthorizedEntity.findOne({ where: [{ email }] });

    if (!authUser) throw new NotFoundException('error.user_not_found');
    let resetToken = await this.jwtService.signAsync({ id: authUser.id, r: 'RESET' })

    Logger.log(resetToken);

    return { message: 'Please check your email for details', status: HttpStatus.CREATED };
  }


  async register(data: RegistrationDTO) {
    const { email, profile, role, username } = data;
    let user = await this.findOneByUsername(username, email);
    if (user) {
      let error = user.username == username ? 'error.username_already_exist' : 'error.email_already_exist';
      throw new BadRequestException(error)
    }
    let newAuth: AuthorizedEntity = new AuthorizedEntity({
      email,
      username,
      password: generatePassword(16),
      roles: [],
      status: UserStatus.RESTRICTED,
    });
    newAuth = await newAuth.save();
    let authRole = new RoleEntity({ role: UserRole.PATIENT, userId: newAuth.id });
    let authProfile = new UserEntity({ ...profile, id: newAuth.id });
    let patientProfile = new PatientEntity({ id: newAuth.id })
    authRole = await authRole.save();
    authProfile = await authProfile.save();
    patientProfile = await patientProfile.save();
    return { message: 'Account created successfully, Please follow the instructions sent to your email', error: null, status: HttpStatus.CREATED };
  }

  async findOneByUsername(username?: string, email?: string) {
    return await AuthorizedEntity.findOne({ where: [{ username }, { email }] });
  }
}
