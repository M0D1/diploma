import { ApiProperty } from "@nestjs/swagger";

export class Token {
    @ApiProperty({ type: String })
    readonly access_token: string;
    @ApiProperty({ type: String, example: 'Bearer' })
    readonly type: string;
}