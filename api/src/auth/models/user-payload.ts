import { UserRole } from 'src/users/models/user-roles.enum';

export type TokenType = "AC" | "RF" | "RE";
export class UserPayload {
  id: string;
  roles: (UserRole | string)[];
  type: TokenType;
}
