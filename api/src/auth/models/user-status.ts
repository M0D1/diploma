export enum UserStatus {
  ACTIVE,
  BLOCKED,
  RESTRICTED,
  SUSPENDED,
}
