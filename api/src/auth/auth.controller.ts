import { Body, Post, Query } from '@nestjs/common';
import { ApiBadRequestResponse, ApiOperation, ApiQuery, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { IResponseError } from 'src/common/interfaces/response.error.interface';

import { AuthService } from './auth.service';
import { LoginDTO, RegistrationDTO } from './dto/auth.dto';
import { PasswordResetDTO } from './dto/password.dto';
import { Token } from './models/token';

@CustomController('Auth', [])
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('login')
  @ApiOperation({ summary: 'Login' })
  @ApiBadRequestResponse({ type: IResponseError })
  @ApiUnauthorizedResponse({ type: IResponseError })
  async signIn(@Body() signInDTO: LoginDTO): Promise<Token> {
    return this.authService.login(signInDTO);
  }

  @Post('register')
  @ApiOperation({ summary: 'Register' })
  async register(@Body() registerDTO: RegistrationDTO) {
    return this.authService.register(registerDTO);
  }


  @Post('request-password')
  @ApiOperation({ summary: 'Request reset password' })
  async requestNewPassword(@Body() data: PasswordResetDTO) {
    return await this.authService.resetPasswordRequest(data);
  }

  @Post('change-password')
  @ApiOperation({ summary: 'Confirm Change Password' })
  @ApiQuery({ type: String, required: true, name: 'token' })
  async changePassword(@Query('token') token: string) {
    return this.authService.changePassword(token);
  }
}
