import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { SearchQuery } from "src/admin/models/appointment.search";

export const SQuery = createParamDecorator(
    (data: unknown, ctx: ExecutionContext): SearchQuery => {
        const request = ctx.switchToHttp().getRequest();
        const { query } = request;
        return query;
    }
)


