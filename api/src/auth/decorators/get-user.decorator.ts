import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { SearchQuery } from 'src/admin/models/appointment.search';
import { UserPayload } from '../models';

export const GetUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): UserPayload => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    return user.user;
  },
);

