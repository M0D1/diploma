import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  Logger,
} from '@nestjs/common';
import { AuthorizedEntity } from '../entities';
import { UserStatus } from '../models';

@Injectable()
export class ActiveGuard implements CanActivate {
  private logger: Logger = new Logger(ActiveGuard.name);
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const { user } = request.user;
    let status: UserStatus = ((await AuthorizedEntity.findOne({ where: { id: user.id }, select: ["status"] })).status);
    if (status === UserStatus.BLOCKED)
      throw new ForbiddenException(`error.account.blocked`);
    if (status === UserStatus.RESTRICTED)
      throw new ForbiddenException(`error.account.restrict`, 'please contact system admin');
    return true;
  }
}
