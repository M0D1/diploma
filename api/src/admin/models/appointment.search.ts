import { AppointmentStatus } from "src/appointments/models/appointment.enum";

export interface SearchQuery {
    doctorId: string;
    patientId: string;
    secretaryId: string;
    date?: Date;
    status: AppointmentStatus
}

