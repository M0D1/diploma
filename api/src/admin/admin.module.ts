import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorizedEntity } from 'src/auth/entities';
import { UserEntity } from 'src/users/entities/user.entity';
import { AppointmentService } from './services/appointment.service';
import { UsersService } from './services/users.service';
import { DoctorService } from './services/doctor.service';
import { RolesController, UsersController, AppointmentController } from './controllers';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, AuthorizedEntity])],
  controllers: [AppointmentController, UsersController, RolesController],
  providers: [AppointmentService, UsersService, DoctorService,],
})
export class AdminModule { }
