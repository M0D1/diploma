import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { from } from 'rxjs';
import { AuthorizedEntity, RoleEntity, RoleStatus } from 'src/auth/entities';
import { generatePassword } from 'src/common/utils/gen-password';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserEntity } from 'src/users/entities/user.entity';
import { UserRole } from 'src/users/models/user-roles.enum';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(AuthorizedEntity)
        private readonly authRepo: Repository<AuthorizedEntity>
    ) { }

    async fetchUsers(options: IPaginationOptions, role: UserRole) {
        let qb = this.authRepo.createQueryBuilder('a');
        qb.leftJoinAndSelect('a.roles', 'roles');
        qb.leftJoinAndSelect('a.profile', 'profile');
        qb.select([
            'a.id',
            'profile.id',
            'profile.name',
            'profile.surname',
            'profile.dob',
            'profile.patronymic',
            'roles.role'
        ]);
        if (role) qb.where('roles.role=:role', { role });
        return from(paginate(qb, options));
    }

    async findRoles(options: IPaginationOptions, role: UserRole, status: RoleStatus) {
        const qb = RoleEntity.createQueryBuilder('r');
        qb.leftJoinAndSelect('r.user', 'user');
        qb.leftJoinAndSelect('user.profile', 'profile');
        if (status && role) qb.where({ role, status });

        if (role && !status) qb.where({ role });

        if (!role && status) qb.where({ status })

        qb.select(['r.role', 'r.status', 'user.id', 'profile.name', 'profile.surname'])
        return from(paginate(qb, options))
    }
    async create(data: CreateUserDto): Promise<UserEntity> {
        let credentials: AuthorizedEntity = new AuthorizedEntity({
            username: data.credentials.username,
        });
        credentials.password = generatePassword(18);
        try {
            Logger.log(`password: ${credentials.password}`); // TODO Send to Phone number
            credentials = await credentials.save();
            let profile: UserEntity = new UserEntity(data as Partial<UserEntity>);
            profile.auth = credentials;
            profile = await profile.save();

            return profile;
        } catch (e) {
            throw e;
        }
    }


    async deleteRole(id: string, role: UserRole) {
        let roleDB = await RoleEntity.findOne({ where: { userId: id, role } })
        if (!roleDB) throw new NotFoundException()

        return await roleDB.remove();
    }


    async update(authId: string, data: any) {
        let user = await this.findOne(authId);
        Object.assign(user, data);
        user = await user.save();
        return user;
    }

    async findOne(id: string) {
        let found = await this.authRepo.findOne({ where: { id } });
        if (!found) throw new NotFoundException();
        return found;
    }
    async updateRoleForUser(id: string, role: UserRole, status: RoleStatus) {
        let roleDB = await RoleEntity.findOne({ where: { userId: id, role } });
        if (!roleDB) throw new NotFoundException('error.role_not_found');
        roleDB.status = status;
        roleDB = await roleDB.save();
        return roleDB;
    }
}
