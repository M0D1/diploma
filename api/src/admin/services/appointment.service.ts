import { Injectable, Logger } from '@nestjs/common';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { from } from 'rxjs';
import { AppointmentEntity } from 'src/appointments/entity';
import { ObjectLiteral } from 'typeorm';
import { SearchQuery } from '../models/appointment.search';

@Injectable()
export class AppointmentService {
    async fetchAppointments(sQuery: SearchQuery, options: IPaginationOptions) {
        let where: ObjectLiteral = {};
        Object.keys(sQuery).forEach(key => {
            where[key] = sQuery[key];
        })
        const qb = AppointmentEntity.createQueryBuilder('a');
        qb.where(where);
        qb.leftJoinAndSelect('a.doctor', 'doctor');
        qb.leftJoinAndSelect('a.patient', 'patient');
        qb.leftJoinAndSelect('doctor.user', 'doctorProfile');
        qb.leftJoinAndSelect('patient.user', 'patientProfile');
        qb.select(['a.id', 'a.status', 'a.startDate', 'a.startTime', 'doctorProfile.name', 'doctor.department', 'doctorProfile.surname', 'patient', 'patientProfile.name', 'patientProfile.surname'])
        return from(paginate<AppointmentEntity>(qb, options));
    }
    async fetchAppointment(appointmentId: number) {
        const qb = AppointmentEntity.createQueryBuilder('a');
        qb.where({ id: appointmentId })
        qb.leftJoinAndSelect('a.doctor', 'doctor');
        qb.leftJoinAndSelect('a.patient', 'patient');
        qb.leftJoinAndSelect('doctor.user', 'doctorProfile');
        qb.leftJoinAndSelect('patient.user', 'patientProfile');
        qb.select(['a.id', 'a.status', 'doctorProfile.name', 'a.startDate', 'a.startTime', 'doctor.department', 'doctor.id', 'doctorProfile.surname', 'patient', 'patientProfile.name', 'patientProfile.surname'])
        return await qb.getOne();
    }
}
