import { ApiProperty } from '@nestjs/swagger';
import { PaginationRes } from 'src/common/response/paginate.response';
import { ProfileDTO } from 'src/users/dto/profile.dto';

export class Doctor extends ProfileDTO {
  @ApiProperty({ type: String })
  id: string;
}
export class DoctorsResponse extends PaginationRes<Doctor> {
  @ApiProperty({ type: [Doctor] })
  items: Array<Doctor>;
}
