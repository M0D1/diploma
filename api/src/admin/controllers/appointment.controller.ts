import { DefaultValuePipe, Get, Param, ParseIntPipe, Query } from '@nestjs/common';
import { ApiParam, ApiQuery } from '@nestjs/swagger';
import { AppointmentStatus } from 'src/appointments/models/appointment.enum';
import { Roles } from 'src/auth/decorators';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { UserRole } from 'src/users/models/user-roles.enum';
import { AppointmentService } from '../services';
import { SearchQuery } from '../models/appointment.search';
import { SQuery } from 'src/auth/decorators/search-query';

@CustomController('A/appointment')
@Roles(UserRole.ADMINISTRATOR)
export class AppointmentController {
    constructor(private readonly service: AppointmentService) { }

    @Get()
    @ApiQuery({ type: String, name: 'doctorId', required: false })
    @ApiQuery({ type: String, name: 'patientId', required: false })
    @ApiQuery({ type: String, name: 'secretaryId', required: false })
    @ApiQuery({ type: String, name: 'date', example: 'dd-mm-yyyy', required: false })
    @ApiQuery({ type: Number, name: 'page', required: false })
    @ApiQuery({ type: Number, name: 'limit', required: false })
    @ApiQuery({ type: () => AppointmentStatus, name: 'status', enum: AppointmentStatus, required: false })
    async getAppointments(
        @SQuery() sQuery: SearchQuery,
        @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
        @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
    ) {
        return this.service.fetchAppointments(sQuery, { limit, page });
    }

    @Get("/:id")
    @ApiParam({ type: Number, name: 'id', required: true })
    async getAppointment(
        @Param('id', ParseIntPipe) id: number
    ) {
        return this.service.fetchAppointment(+id);
    }

}
