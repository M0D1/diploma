import { Controller, DefaultValuePipe, Delete, Get, Param, ParseIntPipe, Put, Query } from '@nestjs/common';
import { ApiParam, ApiQuery, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UsersService } from 'src/admin/services';
import { Roles } from 'src/auth/decorators';
import { RoleStatus } from 'src/auth/entities';
import { UserRole } from 'src/users/models/user-roles.enum';

@Controller('a/users/:sid/roles')
@ApiTags('User Roles')
@Roles(UserRole.ADMINISTRATOR)
export class RolesController {
    constructor(private readonly service: UsersService) { }

    @Get('')
    @ApiQuery({ type: 'enum', name: 'status', enum: RoleStatus, required: false })
    @ApiQuery({ type: Number, name: 'limit', required: false, description: 'Items per response' })
    @ApiQuery({ type: Number, name: 'page', required: false, description: 'Page to display' })
    @ApiQuery({ type: 'enum', name: 'role', enum: UserRole, required: false })
    @ApiOperation({ summary: 'Get All roles' })
    async fetchPendingRoles(
        @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
        @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
        @Query('role') role: UserRole,
        @Query('status') statis: RoleStatus,
    ) {
        return this.service.findRoles({ limit, page }, role, statis);
    }

    @Put('/:id/roles')
    @ApiParam({ name: 'id', description: 'User Id', required: true })
    @ApiParam({ type: String, name: 'id', required: true })
    @ApiQuery({ type: 'enum', name: 'role', enum: UserRole, required: true })
    @ApiQuery({ type: 'enum', name: 'status', enum: RoleStatus, required: true })
    @ApiOperation({ summary: 'Update User Role Status' })
    async updateUserRoleStatus(
        @Param('id') id: string,
        @Query('role') role: UserRole,
        @Query('status') status: RoleStatus
    ) {
        return this.service.updateRoleForUser(id, role, status);
    }


    @Delete("/:id/roles")
    @ApiParam({ type: String, name: 'id', required: true })
    @ApiQuery({ type: 'enum', name: 'role', enum: UserRole, required: true })
    @ApiOperation({ summary: 'Delete User Role' })
    async deleteUserRole(@Param('id') id: string, @Query('role') role: UserRole) {
        return this.service.deleteRole(id, role);
    }

}
