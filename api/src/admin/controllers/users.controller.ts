import { Body, DefaultValuePipe, Get,  Param, ParseIntPipe, Put, Query } from '@nestjs/common';
import { ApiOperation,  ApiQuery } from '@nestjs/swagger';

import { Roles } from 'src/auth/decorators';
import { UserStatus } from 'src/auth/models';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { UserRole } from 'src/users/models/user-roles.enum';
import { UsersService } from '../services';

@CustomController('A/users')
@Roles(UserRole.ADMINISTRATOR)
export class UsersController {
    constructor(private readonly service: UsersService) { }

    @Get()
    @ApiQuery({ type: Number, name: 'limit', required: false, description: 'Items per response' })
    @ApiQuery({ type: Number, name: 'page', required: false, description: 'Page to display' })
    @ApiQuery({ type: 'enum', name: 'role', required: false, enum: UserRole })
    @ApiOperation({ summary: 'Get System Users' })
    async get(
        @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number,
        @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
        @Query('role') role: UserRole
    ) {
        return this.service.fetchUsers({ limit, page }, role);
    }

    @Put('/:id')
    @ApiOperation({ summary: 'Update User Status' })
    async updateUser(@Param('id') id: string, @Body() data: any) {
        return this.service.update(id, data);
    }

    @Put('/:id/status')
    @ApiOperation({ summary: 'Update User Status' })
    @ApiQuery({ type: 'enum', name: 'status', enum: UserStatus, required: true })
    async updateUserStatus(@Param('id') id: string, @Query('status') status: UserStatus) {
        return this.service.update(id, { status });
    }

}
