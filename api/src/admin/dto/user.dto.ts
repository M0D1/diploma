import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { RegisterDTO } from 'src/auth/dto/auth.dto';
import { UserStatus } from 'src/auth/models';
import { ProfileDTO } from 'src/users/dto/profile.dto';
import { UserRole } from 'src/users/models/user-roles.enum';

export class CreateUserDTO extends RegisterDTO {
  @IsNotEmpty()
  @Type(() => ProfileDTO)
  @ApiProperty({ type: () => ProfileDTO })
  profile: ProfileDTO;
  @IsOptional()
  @IsEnum(UserRole)
  role: UserRole;
}
export class UpdateUserStatus {
  @IsNotEmpty()
  @IsEnum(UserStatus)
  readonly status: UserStatus;
  @ApiHideProperty()
  profile;
}