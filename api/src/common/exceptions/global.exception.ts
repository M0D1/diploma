import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
  ForbiddenException,
  HttpException,
  HttpStatus,
  Logger,
  NotFoundException,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Request, Response } from 'express';
// TODO , Implement the rest of TypeOrmExceptions
import {
  CannotCreateEntityIdMapError,
  EntityNotFoundError,
  FindRelationsNotFoundError,
  QueryFailedError,
} from 'typeorm';
import { GlobalResponseError } from './global.response.error';

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    let message = (exception as any).message;

    let code: string;
    Logger.error(
      message,
      (exception as any).stack,
      `${request.method} ${request.url}`,
    );
    let status: HttpStatus;
    code = (exception as any).code;
    switch (exception.constructor) {
      case NotFoundException:
        status = HttpStatus.NOT_FOUND;
        message = (exception as NotFoundException).message;
        break;
      case ForbiddenException:
        status = HttpStatus.FORBIDDEN;
        message = (exception as ForbiddenException).message;
        break;

      case BadRequestException:
        status = HttpStatus.BAD_REQUEST;
        const messages = (exception as any).response.message;
        message = messages || (exception as BadRequestException).message;
        break;
      case UnauthorizedException:
        status = HttpStatus.UNAUTHORIZED;
        message = (exception as UnauthorizedException).message;
        break;
      case HttpException:
        status = (exception as HttpException).getStatus();
        break;
      case QueryFailedError: // this is a TypeOrm error
        status = HttpStatus.UNPROCESSABLE_ENTITY;
        message = (exception as QueryFailedError).message;
        break;
      case EntityNotFoundError: // this is another TypeOrm error
        status = HttpStatus.UNPROCESSABLE_ENTITY;
        message = (exception as EntityNotFoundError).message;
        break;
      case CannotCreateEntityIdMapError: // and another
        status = HttpStatus.UNPROCESSABLE_ENTITY;
        message = (exception as CannotCreateEntityIdMapError).message;
        break;
      case FindRelationsNotFoundError:
        status = HttpStatus.BAD_REQUEST;
        message = (exception as FindRelationsNotFoundError).message;
        break;
      case UnprocessableEntityException:
        status = HttpStatus.UNPROCESSABLE_ENTITY;
        message = (exception as UnprocessableEntityException).message;
        break;
      default:
        status = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    response
      .status(status)
      .json(GlobalResponseError(status, message, code, request));
  }
}
