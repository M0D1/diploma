import { BadRequestException } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { FindRelationsNotFoundError } from 'typeorm';

export class EntityRelationError extends FindRelationsNotFoundError {
  constructor(errors: string[]) {
    super(errors);
  }
}

export class BadRelationException extends BadRequestException {
  @ApiProperty({ type: String, name: 'error' })
  error: string;

  @ApiProperty({ type: String, name: 'message' })
  message: string;

  constructor(error?: string) {
    super(error, 'error.invalid_relation');
  }
}
