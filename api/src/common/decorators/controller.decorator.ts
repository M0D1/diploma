/**
 * Decorator for controllers combining
 * Controller Decorator
 * UseGuards Decorator
 * ApiBearerAuth Decorator Swagger decorator
 *
 */
import {
  applyDecorators,
  CanActivate,
  Controller,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard, RolesGuard } from 'src/auth/guards';
import { ActiveGuard } from 'src/auth/guards/status.guard';

export function CustomController(
  name: string,
  guards: (Function | CanActivate)[] = [JwtAuthGuard, ActiveGuard, RolesGuard],
) {
  return applyDecorators(
    ApiTags(name),
    ApiBearerAuth(),
    UseGuards(...guards),
    Controller(name.toLowerCase()),
  );
}
