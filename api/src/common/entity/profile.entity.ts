import { BaseEntity, Column } from 'typeorm';
import { Gender } from '../../users/models/gender.enum';

export class BaseProfileEntity extends BaseEntity {
  @Column({ type: 'varchar', default: '' })
  name: string;

  @Column({ type: 'varchar', default: '' })
  patronymic: string;

  @Column({ type: 'varchar', default: '' })
  surname: string;

  @Column({ type: 'varchar', nullable: true, default: '' })
  avatar: string;

  @Column({ type: 'enum', enum: Gender, nullable: true })
  gender: Gender;

  @Column({ type: 'varchar', nullable: true })
  dob: string;

  @Column({ type: 'varchar', default: '' })
  phone: string;

  @Column({ type: 'varchar', default: '' })
  region: string;

  @Column({ type: 'varchar', default: '' })
  address: string;


  constructor(partial?: Partial<BaseProfileEntity>) {
    super();
    if (partial) Object.assign(this, partial);
  }
}
