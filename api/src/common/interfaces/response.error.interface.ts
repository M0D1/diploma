import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";

export class IResponseError {
  @Type(() => Number)
  @ApiProperty({ type: Number }) statusCode: number;
  @ApiProperty({ type: String }) message: string;
  @ApiProperty({ type: String }) code: string;
  @ApiProperty({ type: Date }) timestamp: string;
  @ApiProperty({ type: String }) path: string;
  @ApiProperty({ type: String }) method: string;
}
