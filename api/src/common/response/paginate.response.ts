import { ApiProperty } from '@nestjs/swagger';

export class MetaData {
  @ApiProperty({
    type: Number,
    description: 'Total Records in database',
  })
  totalItems: number;
  @ApiProperty({ type: Number }) itemCount: number;
  @ApiProperty({ type: Number }) itemsPerPage: number;
  @ApiProperty({ type: Number }) totalPages: number;
  @ApiProperty({ type: Number }) currentPage: number;
}

export class PaginationRes<T> {
  items: Array<T>;
  @ApiProperty({ type: MetaData })
  meta: MetaData;
}
