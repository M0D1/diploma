export const generatePassword = (passwordLength: number) => {
  let password = '';
  const symbols =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_{}[]';
  for (let i = 0; i < passwordLength; i++) {
    password += symbols.charAt(Math.floor(Math.random() * symbols.length));
  }
  return password;
};
