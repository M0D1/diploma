import { Department } from "src/doctor/entity/doctor.entity";
import { UserEntity } from "src/users/entities/user.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryColumn } from "typeorm";
@Entity('secretaries')
export class SecretaryEntity extends BaseEntity {
    @PrimaryColumn('uuid')
    id: string;

    @OneToOne(() => UserEntity, user => user)
    @JoinColumn({ name: 'id', referencedColumnName: 'id' })
    user: UserEntity;

    @Column({ default: '' })
    office: string;

    @Column({ type: 'enum', enum: Department, default: Department.none, nullable: true })
    department: string;

    @Column({ type: 'jsonb', default: '{}', nullable: true })
    workingHours: string;

    @Column({ type: 'jsonb', default: '{}', nullable: true })
    workingdDays: string;
    
    constructor(partial?: Partial<SecretaryEntity>) {
        super();
        if (partial) Object.assign(this, partial);
    }


}
