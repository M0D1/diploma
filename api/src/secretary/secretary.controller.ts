import { Controller, Get, Post, Body, Patch, Param, Delete, Put, ParseIntPipe, Query } from '@nestjs/common';
import { SecretaryService } from './secretary.service';
import { CreateSecretaryDto } from './dto/create-secretary.dto';
import { UpdateSecretaryDto } from './dto/update-secretary.dto';
import { CustomController } from 'src/common/decorators/controller.decorator';
import { GetUser, Roles } from 'src/auth/decorators';
import { UserRole } from 'src/users/models/user-roles.enum';
import { ApiOperation, ApiParam, ApiQuery } from '@nestjs/swagger';
import { UserPayload } from 'src/auth/models';
import { CreateAppointment } from 'src/appointments/dto/appointment.dto';
import { AppointmentStatus } from 'src/appointments/models/appointment.enum';

@CustomController('Secretary')
@Roles(UserRole.ADMINISTRATOR, UserRole.SECRETARY)
export class SecretaryController {
  constructor(private readonly service: SecretaryService) { }

  @Post('patient')
  @ApiOperation({ summary: 'Create Patient Profile' })
  async createPatientProfile(
    @GetUser() user: UserPayload,
    @Body() data: any
  ) {

  }

  @Post('appointments')
  @ApiOperation({ summary: 'Create Pateint Appointments', description: 'Create an appointments' })
  async getAppointments(@GetUser() user: UserPayload, @Body() data: CreateAppointment) {
    return this.service.makeAppointment(user, data);
  }

  @Put('appointments/:id/status')
  @ApiOperation({ summary: 'Update Pateint Appointment status', description: 'Update an appointment status ' })
  @ApiQuery({ type: 'enum', enum: AppointmentStatus, required: true, name: 'status' })
  @ApiParam({ name: 'id', description: 'AppointmentId', type: Number, required: true })
  async updateAppointment(
    @GetUser() user: UserPayload,
    @Param('id', ParseIntPipe) appointmentId: number,
    @Query('status') status: AppointmentStatus
  ) {
    return this.service.updateAppointmentStatus(user, appointmentId, status);
  }


  // @Post()
  // create(@Body() createSecretaryDto: CreateSecretaryDto) {
  //   return this.secretaryService.create(createSecretaryDto);
  // }
  // @Get()
  // findAll() {
  //   return this.secretaryService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.secretaryService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateSecretaryDto: UpdateSecretaryDto) {
  //   return this.secretaryService.update(+id, updateSecretaryDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.secretaryService.remove(+id);
  // }
}
