import { PartialType } from '@nestjs/swagger';
import { CreateSecretaryDto } from './create-secretary.dto';

export class UpdateSecretaryDto extends PartialType(CreateSecretaryDto) {}
