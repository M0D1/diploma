import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty } from "class-validator";
import { ProfileDTO } from "src/users/dto/profile.dto";

export class CreatePatientDTO {

    @Type(() => ProfileDTO)
    @IsNotEmpty()
    @ApiProperty({ type: ProfileDTO })
    profile: ProfileDTO;
    
}
