import { Injectable } from '@nestjs/common';
import { CreateAppointment } from 'src/appointments/dto/appointment.dto';
import { UserPayload } from 'src/auth/models';
import { CreateSecretaryDto } from './dto/create-secretary.dto';
import { UpdateSecretaryDto } from './dto/update-secretary.dto';

@Injectable()
export class SecretaryService {


  create(createSecretaryDto: CreateSecretaryDto) {
    throw new Error('Method not implemented.');
  }

  findAll() {
    throw new Error('Method not implemented.');
  }

  findOne(id: number) {
    throw new Error('Method not implemented.');
  }

  update(id: number, updateSecretaryDto: UpdateSecretaryDto) {
    throw new Error('Method not implemented.');
  }
  remove(id: number) {
    throw new Error('Method not implemented.');
  }

  makeAppointment(user: UserPayload, data: CreateAppointment) {
    throw new Error('Method not implemented.');
  }

  updateAppointmentStatus(user: UserPayload, appointmentId: number, data?: any) {
    throw new Error('Method not implemented.');
  }
}
