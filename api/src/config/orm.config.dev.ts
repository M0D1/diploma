import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModuleAsyncOptions } from '@nestjs/typeorm';
import { AppointmentEntity } from 'src/appointments/entity/appointment.entity';
import { AuthorizedEntity, RevokedTokensEntity } from 'src/auth/entities';
import { RoleEntity } from 'src/auth/entities/roles.entity';
import { DoctorEntity } from 'src/doctor/entity/doctor.entity';
import { MedicalTestEntity } from 'src/patient/entity/medical-test.entity';
import { PatientEntity } from 'src/patient/entity/patient.entity';
import { SecretaryEntity } from 'src/secretary/entities/secretary.entity';
import { RelativesEntity } from 'src/users/entities/relatives.entity';
import { UserEntity } from 'src/users/entities/user.entity';

export const devOrmConfig: TypeOrmModuleAsyncOptions = {
  imports: [ConfigModule],
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => ({
    type: 'postgres',
    host: configService.get('POSTGRES_HOST'),
    username: configService.get('POSTGRES_USER'),
    password: configService.get('POSTGRES_PASSWORD'),
    database: configService.get('POSTGRES_DB'),
    port: configService.get<number>('POSTGRES_PORT'),
    // synchronize: configService.get('MODE') === 'development',
    // dropSchema: configService.get('MODE') === 'development',
    entities: [
      AuthorizedEntity,
      RoleEntity,
      MedicalTestEntity,
      RevokedTokensEntity,
      UserEntity,
      DoctorEntity,
      SecretaryEntity,
      PatientEntity,
      RelativesEntity,
      AppointmentEntity,
      RevokedTokensEntity
    ],
    migrationsTableName: 'migrations',
    migrations: ['migrations/*.ts'],
  }),
};
