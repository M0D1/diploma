// import { AppointmentEntity } from "src/appointments/entity/appointment.entity";
// import { AuthorizedEntity, RevokedTokensEntity } from "src/auth/entities";
// import { RoleEntity } from "src/auth/entities/roles.entity";
// import { DoctorEntity } from "src/doctor/entity/doctor.entity";
// import { MedicalTestEntity } from "src/patient/entity/medical-test.entity";
// import { PatientEntity } from "src/patient/entity/patient.entity";
// import { SecretaryEntity } from "src/secretary/entities/secretary.entity";
// import { RelativesEntity } from "src/users/entities/relatives.entity";
// import { UserEntity } from "src/users/entities/user.entity";
// import { DataSource } from "typeorm";

// export const AppDataSource: DataSource = new DataSource({
//     type: "postgres",
//     host: process.env.POSTGRES_HOST,
//     port: +process.env.POSTGRES_PORT,
//     username: process.env.POSTGRES_USER,
//     password: process.env.POSTGRES_PASSWORD,
//     database: process.env.POSTGRES_DB,
//     synchronize: true,
//     logging: true,
//     entities: [
//         AuthorizedEntity,
//         RoleEntity,
//         MedicalTestEntity,
//         RevokedTokensEntity,
//         UserEntity,
//         DoctorEntity,
//         SecretaryEntity,
//         PatientEntity,
//         RelativesEntity,
//         AppointmentEntity,
//     ],
//     subscribers: [],
//     migrations: ['migrations/*.ts'],
//     migrationsTableName: "migrations"
// })
// //
