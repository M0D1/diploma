import { ormConfig } from './orm.config';
import fs = require('fs');

fs.writeFileSync(
  'ormconfig.json',
  JSON.stringify(ormConfig.getTypeOrmConfig(), null, 2),
);
